// struct for card games
// Tami Sweitzer, Part 2


#include <iostream>
//#include <conio.h> 
/* 
Suddently conio does not work?
Severity	Code	Description	Project	File	Line	Suppression State
Error	MSB8036	The Windows SDK version 10.0.17763.0 was not found. Install the required version of Windows SDK or change the SDK version in the project property pages or by right-clicking the solution and selecting "Retarget solution".	Cards	C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\Common7\IDE\VC\VCTargets\Microsoft.Cpp.WindowsSDK.targets	46	
The retarget solution > versionNumber did not work. Tried all of them
*/




enum Rank // ace high
{
	TWO = 2, THREE, FOUR, FIVE, SIX, SEVEN, EIGHT, NINE, TEN, JACK, QUEEN, KING, ACE
};

enum Suit
{
	SPADE, DIAMOND, CLUB, HEART
};

struct Card
{
	Rank rank;
	Suit suit;
};


// Functions
void PrintCard(Card card)
{
	// prints rank and suit of card
	std::cout << "This is the " << card.rank << " of " << card.suit << std::endl;
}

Card HighCard(Card card1, Card card2)
{
	if (card1.rank > card2.rank)
	{
		std::cout << "The " << card1.rank << " of " << card1.suit << " is the high card." << std::endl;
		return card1;
	}
	else if (card1.rank < card2.rank)
	{
		std::cout << "The " << card2.rank << " of " << card2.suit << " is the high card." << std::endl;
		return card2;
	}
	else if (card1.rank == card2.rank)
	{
		std::cout << "The cards are equal" << std::endl;
		return card1;	// What would this return?
	}
	else
	{
		std::cout << "The high card cannot be determined" << std::endl;
		return card1;
	}

}



int main()
{
	// testing

	Card card1;
	card1.rank = SIX;
	card1.suit = HEART;

	Card card2;
	card2.rank = QUEEN;
	card2.suit = SPADE;


	PrintCard(card1);
	HighCard(card1, card2);		// prints 12 of 0. I don't like this, but solutions online are too complex for now
	

	// _getch();
	/*This doesn't work as a _getch stand in 
	std::cout << "Press any key to exit" << std::endl;
	char c;
	std::cin >> c;
	if (c) { return 0; }
	*/
	return 0;
	
}



